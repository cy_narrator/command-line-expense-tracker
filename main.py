import getpass
from config import *
from prettytable import PrettyTable
from datetime import datetime
from Database import Database
import os
import sys


def get_password(prompt = 'Password: ', debug=False):
    if os.name == "nt" and not debug:
        password = getpass.win_getpass(prompt=prompt)
    elif os.name == 'posix' and not debug:
        password = getpass.unix_getpass(prompt=prompt)
    else:
        if not debug:
            print("\nError: Cannot determine your OS Shell", file=sys.stderr)
            print("In fallback mode, what you type will be displayed as you type...\n")
        else:
            print("\nDebug mode is on, what you type will be shown as you type...")
        password = input(prompt)
    return password


def line(n=20):
    line_draw = ''
    for num in range(n):
        line_draw += '-'
    return line_draw

def clear():
    if os.name == "nt":
        os.system("cls")
    else:
        os.system("clear")

def out():
    print("Bye")
    exit(0)

def comma_remover(items):
    comma_removed_list = []
    for item in items:
        item = item[0]
        comma_removed_list.append(item)
    return comma_removed_list

def balance():
    add_values = comma_remover(db.list(table_name, "Amount", where_clause="Type = 'add'"))
    print(f"Add values {add_values}")
    subtract_values = comma_remover(db.list(table_name, "Amount", where_clause="Type = 'deduct'"))
    print(f"Subtract values {subtract_values}")
    add_value = sum(add_values)
    print(f"sum of all adds is {add_value}")
    subtract_value = sum(subtract_values)
    print(f"sum of all difference value is {subtract_value}")
    print(f"So balance should be {add_value - subtract_value}")
    return add_value - subtract_value


def local_balance(dataset):
    income = 0
    expense = 0
    for data in dataset:
        if data[2] == 'deduct':
            expense += data[3]
        elif data[2] == 'add':
            income += data[3]
    local_balance = income - expense
    return [income, expense, local_balance]


def add():
    amount_to_add = int(input("Enter the amount you want to enter: "))
    category = input("Enter the category for expenditure(Optional): ")
    db.insert(table_name, f'("{today_date_string}","add", {amount_to_add}, {balance() + amount_to_add}, "{category}")', '(Date, Type, Amount, Balance, Category)')
    menu()


def deduct():
    amount_to_deduct = int(input("Enter the amount you want to deduct: "))
    category = str(input("Enter the category for expenditure(Optional): "))
    db.insert(table_name, f'("{today_date_string}","deduct", {amount_to_deduct}, {balance() - amount_to_deduct}, "{category}")', '(Date, Type, Amount, Balance, Category)')
    menu()



def display():
    try:
        ask = int(input("Enter the number of records you want to see: "))
    except ValueError:
        number_of_items = None
    else:
        number_of_items = ask
    if not number_of_items or number_of_items == 0:
        data = db.list(table_name)
        try:
            local_balance_table = comma_remover(db.list(table_name, 'Balance', number_of_records=1, order_by='ID DESC'))[0]
        except IndexError:
            print("\nNo data to show\n")
            menu()
        local_add_table = comma_remover(db.list(table_name, 'Amount', where_clause='Type = "add"'))
        local_deduct_table = comma_remover(db.list(table_name, 'Amount', where_clause='Type = "deduct"'))
        sum_add_table = sum(local_add_table)
        sum_deduct_table = sum(local_deduct_table)
    else:
        data = db.list(table_name, number_of_records=number_of_items, order_by='ID desc')[::-1]
        if len(data) == 0:
            print("\nNo data to show\n")
            menu()
        local_balance_table = local_balance(data)[2]
        sum_add_table = local_balance(data)[0]
        sum_deduct_table = local_balance(data)[1]
    table = PrettyTable()
    table.align = "c"
    table.field_names = ['ID', 'Date and time', 'Type', 'Amount', 'Category']
    table.align['Amount'] = "l"
    for row in data:
        row = list(row)
        row = [row[stuff] for stuff in range(0, len(row) - 1)]
        table.add_row(row)
    print(table)
    print(f"Total income[In shown Table]: {sum_add_table}\nTotal expenditure[In shown Table]: {sum_deduct_table}\n{line()}Balance[In shown Table]: {local_balance_table}{line()}\n{line(68)}")
    menu()

def delete():
    id = None
    id_accepted = False
    while not id_accepted:
        try:
            id = int(input("Enter the id of data you want to delete: "))
        except ValueError:
            id_accepted = False
        else:
            try:
                row = list(db.list(table_name, number_of_records=1, debug=False, where_clause=f"id = '{id}'")[0])
            except IndexError:
                print("That ID does not exist...")
                id_accepted = False
            else:
                id_accepted = True
    table = PrettyTable()
    table.field_names = ['ID', 'Date and time', 'Type', 'Amount', 'Category']
    row_to_add = [row[i] for i in range(len(table.field_names))]
    table.add_row(row_to_add)
    print("Following entry shall be deleted: \n")
    print(table)
    confirm = input("Are you sure to delete this entry? [Type 'yes' to confirm]: ")
    if confirm.lower() == 'yes':
        try:
            db.delete_record(table_name, where_clause=f"id = '{id}'")
        except:
            print("Due to some error, we cannot delete the data you wanted")
        else:
            print("Data deleted\n")
            menu()
    else:
        menu()


def erase():
    try:
        # temp_password = input("Enter the password to confirm [Ctrl + Z(On windows) or Ctrl + D on *NIX system to cancel]: ")
        temp_password = get_password(debug=True)
    except EOFError:
        print("\nCancelled\n")
        menu()
    else:
        temp_db = Database(user=username, password=temp_password, database=None)
        if len(temp_db.errors) > 0:
            print("\nWrong password, try again\n")
            temp_password = None
            erase()
        else:
            temp_db.errors = []
            temp_password = None
            confirm = input("Again, you are about to delete all the data present here, everything from start till now[Type yes to confirm]: ")
            if confirm == 'yes'.lower():
                db.delete_all_data(table_name)
                print("\n Table erased")
                temp_db = None
                menu()
            print("\nCancelled\n")
            menu()


def edit():
    pass


def menu():
    # global today_date
    global today_date_string
    # today_date = (datetime.now().year, datetime.now().month, datetime.now().day, datetime.now().hour, datetime.now().minute, datetime.now().second)
    today_date_string = f"{datetime.now().year}-{datetime.now().month}-{datetime.now().day} {datetime.now().hour}:{datetime.now().minute}:{datetime.now().second}"
    available_functions = {
        'add': add,
        'deduct': deduct,
        'display': display,
        'delete': delete,
        'erase all data': erase,
        'edit': edit,
        'exit': out
    }
    available_options = [options for options in available_functions]
    correct_option_selected = False
    while not correct_option_selected:
        print(available_options)
        option = input("Enter the option: ").lower()
        if option in available_options:
            correct_option_selected = True
            available_functions[option]()
        else:
            print("\nInvalid Option\n")


def authenticate():
    global db
    authentication_successful = False
    while not authentication_successful:
        print(f"username: {username}")
        # password = input("Enter your password: ")
        password = get_password(debug=False)
        db = Database(user=username, password=password, database=database_name, host=db_host, port=db_port)
        if db.connected:
            authentication_successful = True
            clear()
            password = None
            print("Logged In")
            print("Welcome")
            menu()
        else:
            print(f"\n{db.errors[0]}\n")


authenticate()
