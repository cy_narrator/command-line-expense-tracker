import mysql.connector

class Database:
    def __init__(self, user, password, database = None, host="127.0.0.1", port=3306):
        self.host = host
        self.username = user
        self.password = password
        self.database = database
        self.port = port
        self.cursor = None
        self.connected = False
        self.connection = None
        self.sql = None
        self.errors = []
        try:
            self.connection = mysql.connector.connect(host=self.host, user=self.username, password=self.password, database=self.database, port=self.port)
        except mysql.connector.errors.ProgrammingError:
            self.connected = False
            self.errors.append("Invalid username and/or password")
            self.password = None
            return
        except mysql.connector.errors.DatabaseError:
            self.connected = False
            self.errors.append("Invalid host and/or port")
            self.password = None
            return
        else:
            self.connected = True
            self.password = None  # After the connection is established, there is no need to keep the password lying around
            self.cursor = self.connection.cursor()
            self.errors = []

    def comma_remover(self, items):
        """Not something that belongs here but something that helps alot"""
        comma_removed_list = []
        for item in items:
            item = item[0]
            comma_removed_list.append(item)
        return comma_removed_list

    def delete_record(self,table_name, where_clause):
        table_name = f"{self.database}.{table_name}"
        self.sql = f"DELETE FROM {table_name} WHERE {where_clause}"
        self.execute()
        return f"{self.cursor.rowcount} record(s) deleted"

    def delete_all_data(self, table_name):
        table_name = f"{self.database}.{table_name}"
        self.sql = f"TRUNCATE TABLE {table_name}"
        self.execute()
        return f"Table {table_name} has been completely cleared"

    def edit_record(self, table_name, set_new_data, where_old_data_clause):
        table_name = f"{self.database}.{table_name}"
        self.sql = f"UPDATE {table_name} SET {set_new_data} WHERE {where_old_data_clause}"
        self.execute()
        return f"{self.cursor.rowcount} record(s) affected"

    def delete_table(self, table_name):
        table_name = f"{self.database}.{table_name}"
        self.sql = f"DROP TABLE {table_name}"
        self.execute()

    def create_table(self,table_name, rows):
        """Rows must be a tuple"""
        table_name = f"{self.database}.{table_name}"
        self.sql = f"CREATE TABLE {table_name} {rows}"


    def list(self, table_name, row_names = "*", number_of_records = None, where_clause = None, order_by = None, debug=False):
        """Returns the list of items in the table as a list of tuples, row_names should be in tuple or it will return everything by default"""
        table_name = f"{self.database}.{table_name}"
        if where_clause and order_by and number_of_records:
            self.sql = f"SELECT {row_names} from {table_name} WHERE {where_clause} ORDER BY {order_by} LIMIT {number_of_records} "
            if debug:
                print(f"About to execute {self.sql}")
            self.cursor.execute(self.sql)
            my_result = self.cursor.fetchall()
            output = []
            for x in my_result:
                output.append(x)
            return output

        elif where_clause and number_of_records:
            """Deletes record(s) and returns the status on how many records were deleted"""
            self.sql = f"SELECT {row_names} from {table_name} WHERE {where_clause} LIMIT {number_of_records} "
            if debug:
                print(f"About to execute {self.sql}")
            self.cursor.execute(self.sql)
            my_result = self.cursor.fetchall()
            output = []
            for x in my_result:
                output.append(x)
            return output

        elif order_by and number_of_records:
            self.sql = f"SELECT {row_names} from {table_name} ORDER BY {order_by} LIMIT {number_of_records}"
            if debug:
                print(f"About to execute {self.sql}")
            self.cursor.execute(self.sql)
            my_result = self.cursor.fetchall()
            output = []
            for x in my_result:
                output.append(x)
            return output

        elif where_clause:
            self.sql = f"SELECT {row_names} from {table_name} WHERE {where_clause}"
            if debug:
                print(f"About to execute {self.sql}")
            self.cursor.execute(self.sql)
            my_result = self.cursor.fetchall()
            output = []
            for x in my_result:
                output.append(x)
            return output

        elif order_by:
            self.sql = f"SELECT {row_names} from {table_name} ORDER BY {order_by}"
            if debug:
                print(f"About to execute {self.sql}")
            self.cursor.execute(self.sql)
            my_result = self.cursor.fetchall()
            output = []
            for x in my_result:
                output.append(x)
            return output

        elif number_of_records:
            self.sql = f"SELECT {row_names} from {table_name} LIMIT {number_of_records}"
            if debug:
                print(f"About to execute {self.sql}")
            self.cursor.execute(self.sql)
            my_result = self.cursor.fetchall()
            output = []
            for x in my_result:
                output.append(x)
            return output

        else:
            self.sql = f"SELECT {row_names} from {table_name}"
            if debug:
                print(f"About to execute {self.sql}")
            self.cursor.execute(self.sql)
            my_result = self.cursor.fetchall()
            output = []
            for x in my_result:
                output.append(x)
            return output

    def execute(self):
        """This is probably not what you expect, have a look at query method"""
        print(f"About to execute: {self.sql}")
        self.cursor.execute(self.sql)
        self.connection.commit()

    def query(self, query):
        """Run a custom query passed in as string without semicolon at the end, but dont run SELECT commands, for that use the list method"""
        self.sql = query
        self.execute()

    def insert(self, table_name, table_values, table_rows = None):
        """INSERT INTO TABLE table_name(table_rows) VALUES (table_value)
        Note that table_rows is optional so it is placed at the end
        table_value and table_rows should both be a tuple"""
        table_name = f"{self.database}.{table_name}"
        if not table_rows:
            self.sql = f"INSERT INTO {table_name} values{table_values}"
            self.execute()
        else:
            self.sql = f"INSERT INTO {table_name}{table_rows} values{table_values};"
            self.execute()

