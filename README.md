# Expense tracker

This is a simple expense tracker that uses MySQL to store expense information.

The security of this application is entirely dependent on the security of your MySQL database

# TO set this up

1: ```pip install -r requirements.txt``
2: Change config.py with what you want
3: Just run it